from JobBoard.JobSearch import JobSearch
from JobBoard.job_holder import JobHolder
keywords = "Android Developer"
location = "London"


def get_jobs_from_board(name,keyword, location):
    board = JobSearch(name)
    board.setup()
    all_jobs = []
    all_pages = board.get_search_page(word=keyword, location=location)
    for page in all_pages:
        all_jobs = all_jobs + board.get_job_information(file=name,page=page, keyword=keyword, location=location)
    return all_jobs


jobs = get_jobs_from_board(name='jobsite', keyword=keywords, location=location)
# jobs = get_indeed(keyword=keywords, location=location)
if jobs is None:
    print('================================================================================')
    print('No Jobs Found')
    print('================================================================================')
else:
    for job in jobs:
        print('================================================================================')
        print(job.get_title())
        print(job.get_link())
        print('================================================================================')