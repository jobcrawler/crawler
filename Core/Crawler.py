import requests
from bs4 import BeautifulSoup
from JobBoard.job_holder import JobHolder


class Crawler(object):
    def __init__(self, name):
        self.name = name

    def extract(self, url):
        if url == "":
            return [0, ""]
        else:
            response = requests.get(url)
            if response.text == '':
                return [0, response.status_code]
            else:
                self.response = response
                self.page = BeautifulSoup(response.text, "html.parser", )
                return [1, '']

    def init_html(self, page):
        self.page = BeautifulSoup(page, "html.parser")

    def get_attrs(self, item):
        if len(item) == 0:
            return ''
        else:
            return {item[0]: item[1]}

    def loop_elements(self, config, snippet):
        if snippet == None:
            snippet = self.page

        for con in config:
            keys = con.keys()
            for key in keys:
                item = con[key]
                if key == 'href':
                    snippet = snippet.find('a')
                    break
                else:
                    attr = self.get_attrs(item)
                    if snippet == None:
                        break
                    snippet = snippet.find(key, attr)
        return snippet

    def get_all_elements(self, snippet, element, optionname="", textreturn=False):
        if snippet == '':
            snippet = self.page

        response = snippet.find_all(element, class_=optionname)
        if textreturn:
            return response.getText()
        else:
            return response

    def get_all_job_containers(self, container_config):
        container = []
        loop = container_config['loop']
        snippet = self.loop_elements(config=loop, snippet=None)
        if snippet != None:
            find_all = container_config['find_all']
            for all in find_all:
                keys = all.keys()
                for key in keys:
                    item = all[key]
                    if len(item) == 0:
                        option = ''
                    else:
                        option = item[1]
                    container = container + self.get_all_elements(snippet=snippet, element=key, optionname=option)

        return container

    def get_job_information(self, config, keyword, location):
        data = []
        job_holder = JobHolder()
        main_keys = config.keys()
        for key in main_keys:
            job_config = config[key]
            response = self.get_element_data(key=key, config=job_config)
            if (response[0] == 0):
                job_holder = None
                break  # break this one. Incomplete data otherwise
            else:
                res = response[1]
                if res == None:
                    return None
                else:
                    if key == 'job_title':
                        check = self.element_contains(res, keyword)
                    elif key == 'job_location':
                        check = self.element_contains(res, location)
                    else:
                        check = True
                    if check:
                        job_holder.set_job(key, res)
                    else:
                        job_holder = None
                        break
        if job_holder is not None:
            job_holder.set_jobboard(self.name)
        return job_holder

    def get_element_data(self,key, config):
        element = self.loop_elements(snippet=None, config=config)
        if element == None:
            return [0, '']
        elif element != '' and key == 'job_link' and element.has_attr('href'):
            return [1, element['href']]
        elif element != 0:
            return [1, element.text.rstrip().strip()]
        else:
            return [0, '']

    def element_contains(self, element, contains):
        split = []
        if ' ' in contains:
            split = contains.split()
        else:
            split.append(contains)

        flag = 0
        # Check ALL words are found
        for s in split:
            if s in element:
                flag = 1
            else:
                flag = 0
                break
        return flag
