import mechanicalsoup
import requests


class Search(object):
    def __init__(self, url):
        self.browser = mechanicalsoup.Browser()
        # Get the main page
        self.headers = {'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.102 Safari/537.36'}
        self.page = self.browser.get(url, headers=self.headers)

    def login(self, ):
        # Implement a login
        i = 0

    def search(self, form, search_criteria):
        # self.get_form(form)
        search_form = mechanicalsoup.Form(self.page.soup.form)
        for search in search_criteria:
            type = search[0]
            find_by_name = search[1]
            value = search[2]
            if type == 'text':
                search_form.input({find_by_name: value})
            if type == 'checkbox':
                search_form.check({find_by_name})
        response = self.browser.submit(search_form, self.page.url, headers=self.headers)
        if response.status_code == 200:
            return {'success': 1, 'response': response}
        else:
            return {'success': 0}

    def indeed_get_all_pages(self, page):
        x = 2
        pages = []
        pages.append(page.text)
        while x < 2:
            url = page.url + "&start=" + str(x * 10)
            response = requests.get(url)
            print("Response from" + url)
            if response.status_code != 200:
                break
            else:
                response_text = response.text
                if "could not be found" in response_text:
                    break
                else:
                    pages.append(response_text)
            x += 1
        return pages


    def monster_get_all_pages(self, page):
        x = 2
        pages = []
        pages.append(page.text)
        while x < 50:
            url = page.url + "&page=" + str(x)
            response = requests.get(url)
            print("Response from" + url)
            if response.status_code != 200:
                break
            else:
                response_text = response.text
                if "We couldn't find this page for you" in response_text:
                    break
                else:
                    pages.append(response_text)
            x += 1
        return pages
