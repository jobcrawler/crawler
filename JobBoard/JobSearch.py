from Core.Search import Search
from Core.Crawler import Crawler
import requests
import json


class JobSearch(object):
    def __init__(self, name):
        self.crawler = Crawler(name)
        self.name = name

    def setup(self):
        with open('files/sites.json') as data_file:
            data = json.load(data_file)
        job_data = data[self.name]
        self.url = job_data['url']
        self.search = Search(self.url)
        self.search_fields = job_data['search']

    def get_config(self, configfile):
        with open('files/' + configfile + '.json') as data_file:
            data = json.load(data_file)
        return data

    def get_search_page(self, word, location):
        search = []
        all_pages = []
        keyword_field = self.search_fields['keyword']
        location_field = self.search_fields['location']
        search.append([keyword_field[0], keyword_field[1], word])
        search.append([location_field[0], location_field[1], location])
        response = self.search.search(form=self.search_fields['form'], search_criteria=search)
        if response['success'] == 0:
            print("Error")
        else:
            if self.name == 'monster':
                all_pages = self.search.monster_get_all_pages(response['response'])
            if self.name == 'indeed':
                all_pages = self.search.indeed_get_all_pages(response['response'])
            else:
                all_pages.append(response['response'].text)
            return all_pages

    def get_job_information(self, file, page, keyword, location):
        results = []
        self.crawler.init_html(page)
        config = self.get_config(file)
        container_config = config['container']
        config.pop('container', None)  # Don't need this anymore

        job_containers = self.crawler.get_all_job_containers(container_config=container_config)
        for container in job_containers:
            self.crawler.init_html(self.list_to_string(container.contents))
            res = self.crawler.get_job_information(config=config, keyword=keyword, location=location)
            if res != None:
                results.append(res)
        return results

    def list_to_string(self, list):
        list_string=''
        for l in list:
            list_string = list_string + ' ' + str(l)
        return list_string
