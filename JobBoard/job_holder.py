class JobHolder(object):

    def __init__(self):
        self.title=''
        self.link=''
        self.expiry=''
        self.jobboard=''

    def set_job(self, key, value):
        key_options = {
            'job_title' : self.set_title,
            'job_link' : self.set_link,
            'job_board' : self.set_jobboard,
            'job_location' : self.set_location,
        }
        key_options[key](value)

    def set_location(self, location):
        self.location = location

    def set_title(self, title):
        self.title = title

    def set_link(self, link):
        self.link = link

    def set_expiry(self, expiry):
        self.expiry = expiry

    def set_jobboard(self, jobboard):
        self.jobboard = jobboard

    def get_location(self):
        return self.location

    def get_title(self):
        return self.title

    def get_link(self):
        return self.link

    def get_expiry(self):
        return self.expiry

    def get_jobboard(self):
        return self.jobboard