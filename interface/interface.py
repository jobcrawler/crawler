from flask import Flask, render_template, request
from JobBoard.JobSearch import JobSearch

app = Flask(__name__)


@app.route('/')
def index():
    return render_template('index.html')

@app.route('/search', methods=["POST"])
def search():
    print("Search")
    keyword = request.form['keyword-text']
    location = request.form['location-text']
    jobs = get_jobs_from_board(keyword=keyword, location=location)
    return render_template('search.html', jobs=jobs)

def get_jobs_from_board(keyword, location):
    names = ['monster', 'indeed']
    all_jobs = []
    for name in names:
        board = JobSearch(name)
        board.setup()
        all_jobs = []
        all_pages = board.get_search_page(word=keyword, location=location)
        for page in all_pages:
            all_jobs = all_jobs + board.get_job_information(file=name,page=page, keyword=keyword, location=location)
    return all_jobs






if __name__ == '__main__':
    app.run()
